import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { CarauselComponent } from './carausel/carausel.component';
import { AdminComponent } from './admin/admin/admin.component';
import { OtpComponent } from './otp/otp.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';


const routes: Routes = [{
  path: "",
  redirectTo: "home/carausel",
  pathMatch: "full"
},
{
  path: "home",
  component: HomeComponent,
  children: [
    {
      path: 'carausel',
      component: CarauselComponent

    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'admin',
      component: AdminComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'otp',
      component: OtpComponent
    },
    {
      path: 'forgetpassword',
      component: ForgetpasswordComponent
    },
    {
      path: 'changepassword',
      component: ChangepasswordComponent
    }

  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
