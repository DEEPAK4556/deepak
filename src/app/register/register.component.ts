import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {



  constructor(private route: Router, private http: HttpClient) { }

  ngOnInit() {
  }
  method(data) {


    this.http.post("home/register", data).subscribe((res) => {


      if (res["message"] == 'already exists') {
        alert("username already exists")
      }
      else if (res["message"] == 'success') {
        alert("sucessfully registered")
        this.route.navigate(['home/login'])
      }

    })


  }




}
