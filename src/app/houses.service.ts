import { Injectable } from '@angular/core';
 import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HousesService {
  houseList:any[]=[]
  accept:any
  reject:any
  valueFromWhomTolet: any;
  whomtolet:any[]=[];

  constructor(private houses:HousesService,private http:HttpClient) { }

  setResponse(data):Observable<any[]>
{
  console.log(data)
  return this.http.put<any[]>("/owner/client",data)
 
}
deleteClient(data):Observable<any>
{
  return this.http.delete<any>('/owner/client',data)
}

}
