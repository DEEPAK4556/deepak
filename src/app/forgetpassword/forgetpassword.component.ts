import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  submit(data) {


    this.http.post('home/forgetpassword', data).subscribe(res => {

      if (res['message'] == "user found") {
        alert('user found')
        this.router.navigate(['/home/otp'])
      }
      else {
        alert(res['message'])
      }
    })
  }


}
