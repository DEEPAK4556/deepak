import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OwnerModule } from './owner/owner.module';
import { CarauselComponent } from './carausel/carausel.component';
import { VendorModule } from './vendor/vendor.module';
import { AdminModule } from './admin/admin.module';
import { OwnerdbModule } from './owner/ownerdb/ownerdb.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthorizationService } from './authorization.service';
import { OtpComponent } from './otp/otp.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarauselComponent, LoginComponent, RegisterComponent, OtpComponent, ForgetpasswordComponent, ChangepasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, OwnerModule, VendorModule, AdminModule, FormsModule, OwnerdbModule, HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthorizationService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
