import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements HttpInterceptor {

  constructor() { }
       intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
        {
          //read token from the local storage

          const idtoken = localStorage.getItem("idToken")

          if(idtoken)
          {
                const cloned = req.clone({headers:req.headers.set("Authorization","Bearer "+ idtoken)})
                return next.handle(cloned)
          }
          else
          {
            return next.handle(req)
          }
        }
}
