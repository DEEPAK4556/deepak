import { TestBed } from '@angular/core/testing';

import { StatusofpaymentService } from './statusofpayment.service';

describe('StatusofpaymentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatusofpaymentService = TestBed.get(StatusofpaymentService);
    expect(service).toBeTruthy();
  });
});
