import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StatusofpaymentService {
  valueFromWhomTolet: boolean;
  status: boolean;
  check: boolean;

  editprofiles: any;




  constructor(private http: HttpClient, private register: StatusofpaymentService) { }

  vendorpaymenthistory(): Observable<any[]> {
    return this.http.get<any[]>('vendor/paymenthistory')
  }
  ownerpaymenthistory(): Observable<any[]> {
    return this.http.get<any[]>('owner/paymenthistory')
  }

  viewpayment(): Observable<any[]> {
    return this.http.get<any[]>('/owner/viewpayment')
  }






}
