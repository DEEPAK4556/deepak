import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendordbModule } from './vendordb/vendordb.module';
import { FormsModule } from '@angular/forms'
import { VendorRoutingModule } from './vendor-routing-module';




@NgModule({
  declarations: [],
  imports: [
    CommonModule, VendordbModule, VendorRoutingModule, FormsModule
  ]
})
export class VendorModule { }
