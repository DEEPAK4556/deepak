import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { VendordbRoutingModule } from './vendordb-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { WhomToLetComponent } from './whom-to-let/whom-to-let.component';
import { PaymentsComponent } from './payments/payments.component';
import { AddprofileComponent } from './profile/addprofile/addprofile.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { DopaymentComponent } from './payments/dopayment/dopayment.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';
import { VendordbComponent } from './vendordb/vendordb.component';
import { LogoutComponent } from './logout/logout.component';
import { FindPipe } from './find.pipe';

@NgModule({
  declarations: [ProfileComponent, WhomToLetComponent, PaymentsComponent, AddprofileComponent, ViewprofileComponent, DopaymentComponent, PaymenthistoryComponent, VendordbComponent, LogoutComponent, FindPipe],
  imports: [
    CommonModule,
    VendordbRoutingModule, FormsModule
  ]
})
export class VendordbModule { }
