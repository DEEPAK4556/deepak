import { Component, OnInit } from '@angular/core';
import { HousesService } from 'src/app/houses.service';
import { AddhouseService } from 'src/app/owner/ownerdb/addhouse/addhouse.service';
import { HttpClient } from '@angular/common/http';

import { StatusofpaymentService } from 'src/app/statusofpayment.service';


@Component({
  selector: 'app-whom-to-let',
  templateUrl: './whom-to-let.component.html',
  styleUrls: ['./whom-to-let.component.css']
})
export class WhomToLetComponent implements OnInit {
  b: boolean = true;
  accept: any
  list: any[] = [];
  searchTerm: string;

  whomtolet: any[] = [];
  currentUser: any;


  constructor(private houses: HousesService, private http: HttpClient, private tolet: AddhouseService, private register: StatusofpaymentService) { }

  ngOnInit() {
    /*this.accept=this.houses.accept
    this.accept=this.houses.reject


     
    this.tolet.add().subscribe(data=>{this.whomtolet=data['message']})
    console.log(this.accept)*/


    this.currentUser = this.register.editprofiles
    console.log(this.currentUser)

    this.http.get('/vendor/whom-to-let').subscribe(res => { this.list = res['message'] })
    console.log(this.list)

  }



  changeStatus(expectedRent, house) {
    console.log(expectedRent, house)
    console.log(this.currentUser)
    var insteredHouse =
    {
      "eRent": expectedRent,
      "address": house.address,
      "vendorname": this.currentUser[0].name,
      "number": this.currentUser[0].number,
      "mail": this.currentUser[0].mail,
      "ownername": house.ownername,


    }
    console.log(insteredHouse)
    this.http.post('/vendor/whom-to-let', insteredHouse).subscribe(res => {
      alert(res['message'])
    }
    )




  }
}




