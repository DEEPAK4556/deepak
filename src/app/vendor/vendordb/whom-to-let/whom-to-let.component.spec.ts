import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhomToLetComponent } from './whom-to-let.component';

describe('WhomToLetComponent', () => {
  let component: WhomToLetComponent;
  let fixture: ComponentFixture<WhomToLetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhomToLetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhomToLetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
