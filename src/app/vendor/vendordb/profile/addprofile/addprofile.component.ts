import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';
import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-addprofile',
  templateUrl: './addprofile.component.html',
  styleUrls: ['./addprofile.component.css']
})
export class AddprofileComponent implements OnInit {

  editprofiles: any;

  constructor(private http: HttpClient, private register: StatusofpaymentService) { }

  ngOnInit() {
  this.editprofiles = this.register.editprofiles[0]
  }
  profile(data) {
    console.log(data)
    this.http.put('vendor/addprofile', data).subscribe(res => {

      alert(res['message'])
    })
  }
}




