import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'find'
})
export class FindPipe implements PipeTransform {

  transform(houses: any[], searchTerm: string): any {
    if (!searchTerm) {
      return houses
    }
    return houses.filter(searchedTerm =>
      searchedTerm.Htype.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
      searchedTerm.address.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
      searchedTerm.rent.toString().indexOf(searchTerm.toLowerCase()) !== -1
    )
  }

}
