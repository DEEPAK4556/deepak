import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendordb',
  templateUrl: './vendordb.component.html',
  styleUrls: ['./vendordb.component.css']
})
export class VendordbComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }
  logout() {
    localStorage.removeItem("idToken")
    this.route.navigate(['/home/login'])
  }

}
