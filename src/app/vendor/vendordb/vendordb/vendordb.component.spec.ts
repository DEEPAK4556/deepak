import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendordbComponent } from './vendordb.component';

describe('VendordbComponent', () => {
  let component: VendordbComponent;
  let fixture: ComponentFixture<VendordbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendordbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendordbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
