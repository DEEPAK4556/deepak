import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendordbComponent } from './vendordb/vendordb.component';
import { WhomToLetComponent } from './whom-to-let/whom-to-let.component';
import { ViewprofileComponent } from 'src/app/vendor/vendordb/profile/viewprofile/viewprofile.component';
import { AddprofileComponent } from 'src/app/vendor/vendordb/profile/addprofile/addprofile.component';
import { ProfileComponent } from './profile/profile.component';
import { PaymentsComponent } from './payments/payments.component';
import { DopaymentComponent } from './payments/dopayment/dopayment.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  {
    path: 'vendor',
    component: VendordbComponent,
    children: [{
      path: 'profile',
      component: ProfileComponent
    },
    {
      path: 'addprofile',
      component: AddprofileComponent
    },
    {
      path: 'viewprofile',
      component: ViewprofileComponent
    },
    {
      path: 'whom-to-let',
      component: WhomToLetComponent
    },
    {
      path: 'payments',
      component: PaymentsComponent
    },


    {
      path: 'dopayment',
      component: DopaymentComponent
    },
    {
      path: 'paymenthistory',
      component: PaymenthistoryComponent
    },


    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendordbRoutingModule { }
