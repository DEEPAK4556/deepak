import { Component, OnInit } from '@angular/core';
import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-paymenthistory',
  templateUrl: './paymenthistory.component.html',
  styleUrls: ['./paymenthistory.component.css']
})
export class PaymenthistoryComponent implements OnInit {
  payment: any[]
  constructor(private vendorpayment: StatusofpaymentService) { }

  ngOnInit() {
    this.vendorpayment.vendorpaymenthistory().subscribe(data => { this.payment = data["message"] })
  }

}
