import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dopayment',
  templateUrl: './dopayment.component.html',
  styleUrls: ['./dopayment.component.css']
})
export class DopaymentComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  dopayment(data) {
    console.log(data)
    this.http.post("vendor/dopayment", data).subscribe((res) => {
      alert(res["message"])
    })
  }
}
