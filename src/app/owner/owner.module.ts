import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { OwnerRoutingModule } from './owner-routing.module';

import { from } from 'rxjs';
import { OwnerdbModule } from './ownerdb/ownerdb.module';
import { HttpClientModule } from "@angular/common/http"


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OwnerRoutingModule, FormsModule, OwnerdbModule, HttpClientModule
  ]
})
export class OwnerModule { }
