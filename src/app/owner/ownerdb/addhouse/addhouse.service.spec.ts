import { TestBed } from '@angular/core/testing';

import { AddhouseService } from './addhouse.service';

describe('AddhouseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddhouseService = TestBed.get(AddhouseService);
    expect(service).toBeTruthy();
  });
});
