import { Component, OnInit } from '@angular/core';
import { AddhouseService } from './addhouse.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-addhouse',
  templateUrl: './addhouse.component.html',
  styleUrls: ['./addhouse.component.css']
})
export class AddhouseComponent implements OnInit {
  a: any[] = []
  constructor(private http: HttpClient) { }

  ngOnInit() {

  }
  add(data) {
    console.log(data)
    this.http.post("owner/addhouse", data).subscribe((res) => {
      alert(res["message"])
      //this.a.push(data)
    })
  }
}



