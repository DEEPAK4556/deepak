import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddhouseService {
  addhouse: any[] = []

  paymentshistory: any[] = [];

  constructor(private http: HttpClient) { }
  add(): Observable<any[]> {
    return this.http.get<any[]>('owner/tolet')
  }

  vendorpayments(): Observable<any[]> {
    return this.http.get<any[]>('owner/paymenthistory')
  }
}
