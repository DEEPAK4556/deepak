import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { OwnerdbRoutingModule } from './ownerdb-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import { ToletComponent } from './tolet/tolet.component';
import { ClientComponent } from './client/client.component';
import { PaymentsComponent } from './payments/payments.component';
import { OwnerdbComponent } from './ownerdb.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { AddprofileComponent } from './profile/addprofile/addprofile.component';
import { AddpaymentComponent } from './payments/addpayment/addpayment.component';
import { ViewpaymentComponent } from './payments/viewpayment/viewpayment.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';
import { HttpClientModule } from '@angular/common/http';
import { LogoutComponent } from './logout/logout.component'

@NgModule({
  declarations: [ProfileComponent, AddhouseComponent, ToletComponent, ClientComponent, PaymentsComponent, OwnerdbComponent, ViewprofileComponent, AddprofileComponent, AddpaymentComponent, ViewpaymentComponent, PaymenthistoryComponent, LogoutComponent],
  imports: [
    CommonModule,
    OwnerdbRoutingModule, FormsModule, HttpClientModule
  ]
})
export class OwnerdbModule { }
