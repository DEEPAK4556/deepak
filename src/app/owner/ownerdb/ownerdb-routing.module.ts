import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerdbComponent } from './ownerdb.component';
import { ProfileComponent } from './profile/profile.component';
import { AddprofileComponent } from './profile/addprofile/addprofile.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import { ToletComponent } from './tolet/tolet.component';
import { PaymentsComponent } from './payments/payments.component';
import { AddpaymentComponent } from './payments/addpayment/addpayment.component';
import { ViewpaymentComponent } from './payments/viewpayment/viewpayment.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';
import { ClientComponent } from './client/client.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [{
  path: 'owner',
  component: OwnerdbComponent,
  children: [{
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'editprofile',
    component: AddprofileComponent
  },
  {
    path: 'viewprofile',
    component: ViewprofileComponent
  },
  {
    path: 'addhouse',
    component: AddhouseComponent
  },
  {
    path: 'tolet',
    component: ToletComponent
  },
  {
    path: 'client',
    component: ClientComponent
  },
  {
    path: 'payment',
    component: PaymentsComponent
  },
  {
    path: 'addpayment',
    component: AddpaymentComponent
  },
  {
    path: 'viewpayment',
    component: ViewpaymentComponent
  },
  {
    path: 'paymenthistory',
    component: PaymenthistoryComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerdbRoutingModule { }
