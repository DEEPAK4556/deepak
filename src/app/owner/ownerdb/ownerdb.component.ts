import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Token } from '@angular/compiler';

@Component({
  selector: 'app-ownerdb',
  templateUrl: './ownerdb.component.html',
  styleUrls: ['./ownerdb.component.css']
})
export class OwnerdbComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('idToken')
    this.route.navigate(["/home/login"])
  }

}
