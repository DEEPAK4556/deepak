import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  c: any[] = []
  constructor(private edit: Router, private obj: ProfileService) { }

  ngOnInit() {
  }
  view() {
    this.edit.navigate[('editprofile')]
  }
  viewProfile() {
    this.c = this.obj.b
  }
}
