import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { OwnersService } from 'src/app/owners.service';
import { HttpClient } from '@angular/common/http';
import { RegistredDataService } from 'src/app/registred-data.service';
import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-addprofile',
  templateUrl: './addprofile.component.html',
  styleUrls: ['./addprofile.component.css']
})
export class AddprofileComponent implements OnInit {

  editprofiles: any;

  constructor(private register: StatusofpaymentService, private http: HttpClient, private route: Router) { }

  ngOnInit() {
  this.editprofiles = this.register.editprofiles[0]
    console.log(this.register.editprofiles)
  }


  add(data) {
    console.log(data)
    this.http.put('owner/editprofile', data).subscribe(res => {
      alert(res['message'])

      this.editprofiles = res['message']

      this.route.navigate(['/owner/viewprofile'])
    })
  }
}












