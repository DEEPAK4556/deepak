import { Component, OnInit } from '@angular/core';


import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {
  ownerprofile: any[] = []

  constructor(private register: StatusofpaymentService) { }

  ngOnInit() {
    this.ownerprofile = this.register.editprofiles

  }


}
