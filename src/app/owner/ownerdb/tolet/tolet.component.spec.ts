import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToletComponent } from './tolet.component';

describe('ToletComponent', () => {
  let component: ToletComponent;
  let fixture: ComponentFixture<ToletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
