import { Component, OnInit } from '@angular/core';
import { AddhouseService } from '../addhouse/addhouse.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tolet',
  templateUrl: './tolet.component.html',
  styleUrls: ['./tolet.component.css']
})
export class ToletComponent implements OnInit {
  houses: any[] = []
  del: boolean = false

  constructor(private tolet: AddhouseService, private http: HttpClient) { }

  ngOnInit() {
    this.tolet.add().subscribe(data => { this.houses = data['message'] })

  }

  delete(data): any {
    this.del = true;

    this.http.delete(`owner/tolet/${data}`).subscribe(data => {
      alert('record deleted')



    })
    this.tolet.add().subscribe(data => { this.houses = data['message'] })

  }

}
