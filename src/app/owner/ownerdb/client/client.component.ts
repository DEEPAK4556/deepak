import { Component, OnInit } from '@angular/core';

import { StatusofpaymentService } from '../../../statusofpayment.service';
import { HousesService } from 'src/app/houses.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  acceptRequest: any
  clients: any


  constructor(private http: HttpClient, private houses: HousesService, private register: StatusofpaymentService) { }

  ngOnInit() {//this.fromWhomTolet=this.houses.valueFromWhomTolet



    this.http.get(`/owner/client/${this.register.editprofiles[0].name}`).subscribe(res => {
      this.clients = res['message']
      console.log(this.register.editprofiles)


    })
  }



  accept(data) {
    data.reqstatus = "request accepted";

    this.houses.setResponse(data).subscribe(res => {
      alert('response sent')
      this.clients = res['data']

    })


  }
  reject(data) {
    data.reqstatus = "request rejected";
    this.houses.deleteClient(data).subscribe(res => {
      alert('response sent')
      this.clients = res['data']
    })

  }




}
