import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerdbComponent } from './ownerdb.component';

describe('OwnerdbComponent', () => {
  let component: OwnerdbComponent;
  let fixture: ComponentFixture<OwnerdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
