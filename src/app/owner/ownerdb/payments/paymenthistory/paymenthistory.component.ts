import { Component, OnInit } from '@angular/core';

import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-paymenthistory',
  templateUrl: './paymenthistory.component.html',
  styleUrls: ['./paymenthistory.component.css']
})
export class PaymenthistoryComponent implements OnInit {
  vendorpayments: any[] = []
  constructor(private payments: StatusofpaymentService) { }

  ngOnInit() {
    this.payments.vendorpaymenthistory().subscribe(data => { this.vendorpayments = data["message"] })


  }

}
