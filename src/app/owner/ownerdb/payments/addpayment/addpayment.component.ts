import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addpayment',
  templateUrl: './addpayment.component.html',
  styleUrls: ['./addpayment.component.css']
})
export class AddpaymentComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  pay(data) {

    this.http.post("owner/addpayment", data).subscribe((res) => {
      alert("payment added")
    })

  }
}
