import { Component, OnInit } from '@angular/core';
 
import { StatusofpaymentService } from 'src/app/statusofpayment.service';

@Component({
  selector: 'app-viewpayment',
  templateUrl: './viewpayment.component.html',
  styleUrls: ['./viewpayment.component.css']
})
export class ViewpaymentComponent implements OnInit {
viewpayment:any
  constructor(private register:StatusofpaymentService ) { }

  ngOnInit() { 
    this.register.viewpayment().subscribe(data=>{this.viewpayment=data['message']

    })
    
  }
 
  
}
