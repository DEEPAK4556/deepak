import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adlogin',
  templateUrl: './adlogin.component.html',
  styleUrls: ['./adlogin.component.css']
})
export class AdloginComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  submit(data) {

    this.http.post('/admin/adlogin', data).subscribe(res => {
      alert(res['message'])

      if (res['message'] == 'logged In Successfully') {
        this.router.navigate(['admin/allprofiles'])
      }
    })
  }
  back() {
    this.router.navigate(["home/carausel"])
  }
}
