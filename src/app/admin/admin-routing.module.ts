import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AllprofilesComponent } from './allprofiles/allprofiles.component';

import { AdloginComponent } from './adlogin/adlogin.component';


const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [{
      path: "adlogin",
      component: AdloginComponent
    },
    {
      path: "allprofiles",
      component: AllprofilesComponent

    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
