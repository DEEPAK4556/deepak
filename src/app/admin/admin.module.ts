import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms"

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { AllprofilesComponent } from './allprofiles/allprofiles.component';

import { AdloginComponent } from './adlogin/adlogin.component';

@NgModule({
  declarations: [AdminComponent, AllprofilesComponent, AdloginComponent],
  imports: [
    CommonModule,
    AdminRoutingModule, FormsModule
  ]
})
export class AdminModule { }
