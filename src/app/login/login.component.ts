import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { HttpClient } from '@angular/common/http';
import { StatusofpaymentService } from '../statusofpayment.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  edit: any

  constructor(private route: Router, private http: HttpClient, private register: StatusofpaymentService) { }

  ngOnInit() {
  }
  submit(data) {
    this.http.post('home/login', data).subscribe((res) => {



      if (res["message"] == "entered wrong username") {
        alert("entered wrong owner username")
      }
      else if (res['message'] == "invalid owner password") {
        alert('entered wrong Vendor password')
      }
      else if (res["message"] == 'owner successfully logged in') {
        alert("owner successfully logged in")
        localStorage.setItem("idToken", res['token'])


        this.register.editprofiles = res["ownerdata"]
        console.log(this.register.editprofiles)
        this.route.navigate(['/owner'])
      }


      else if (res["message"] == "entered wrong vendor username") {
        alert("entered wrong vendor username")
      }

      else if (res["message"] == "invalid vendor password") {
        alert("entered wrong password")
      }

      else if (res["message"] == "vendor successfully logged in") {
        localStorage.setItem("idToken", res['token'])
        this.register.editprofiles = res["vendordata"]


        this.route.navigate(['/vendor'])
      }
      else if (res['message'] == 'select usertype') {
        alert('please select usertype')
      }

    })
  }
}




