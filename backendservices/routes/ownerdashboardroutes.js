const exp = require('express')
var ownerdashboardroutes = exp.Router();
const authorization = require('../middleware/checkauthorization')

// importing dbconfig to adminroutes.js

const initdb = require("../dbconfig").initdb
const getdb = require("../dbconfig").getdb
initdb();

// request handlers for admin routes

ownerdashboardroutes.post('/addhouse', (req, res) => {
    console.log(req.body)
    var dbo = getdb();
    dbo.collection('addhouse').insertOne(req.body, (err, res) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ 'message': "House Added" })
        }
    })

})

ownerdashboardroutes.put('/editprofile', (req, res, next) => {
    console.log(req.body)

    var dbo = getdb();

    dbo.collection('owner').updateOne({ name: { $eq: req.body.name } }, {
        $set: {
            name: req.body.name,
            dob: req.body.dob,
            address: req.body.address,
            email: req.body.email,
            mobile: req.body.mobile
        }
    }, (err, modifieddata) => {
        console.log(modifieddata)
        if (err) {
            next(err);
        }

        else {

            res.json({ "message": "profile modified" })
        }
    })
}
)
//get request handler for tolet
ownerdashboardroutes.get('/tolet', (req, res) => {

    var dbo = getdb();
    dbo.collection('addhouse').find().toArray((err, data) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ message: data })
        }
    })

})


// request handlers for admin routes

ownerdashboardroutes.post('/addpayment', (req, res) => {

    var dbo = getdb();
    dbo.collection('addpayment').insertOne(req.body, (err, success) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ message: success })
        }
    })

})


ownerdashboardroutes.get('/viewpayment', (req, res) => {

    var dbo = getdb();
    dbo.collection('addpayment').find().toArray((err, success) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ message: success })
        }
    })

})


//delete request handler for viewhouse

ownerdashboardroutes.delete('/tolet/:ownerrent', (req, res) => {
    console.log(req.params)
    var dbo = getdb();
    dbo.collection('addhouse').deleteOne({ ownerrent: { $eq: req.params.rent } }, (err, data) => {
        if (err) {
            console.log("error in deletion")
            console.log(err)
        }
        else {
            res.json({ message: "record deleted", data })
        }
    })

})

//getting clients who are interseted
//ownerdashboard get method
ownerdashboardroutes.get('/client/:name', (req, res) => {
    console.log(req.params)
    var dbo = getdb();
    dbo.collection("whomtolet").find({ ownername: { $eq: req.params.name } }).toArray((err, data) => {
        if (err) {
            console.log(err)

        }
        else {
            //console.log(data)
            console.log(data)
            res.json({ message: data })

        }
    })
})

//get request handler for viewpayment method




//put request handler for the req,status

ownerdashboardroutes.put('/client', (req, res, next) => {
    console.log(req.body)
    var dbo = getdb();
    dbo.collection("addhouse").updateOne({ address: { $eq: req.body.address } }, { $set: { reqstatus: req.body.reqstatus, vendorname: req.body.vendorname } }, (err, success) => {
        if (err) {
            console.log('error in saving data')
            next(err)
        }
        else {
            dbo.collection("whomtolet").deleteOne({ $and: [{ vendorname: req.body.vendorname }, { address: req.body.address }] }, (err, success) => {
                if (err) {
                    console.log('error in saving data')
                    next(err)
                }
                else {
                    dbo.collection('whomtolet').find({ ownername: { $eq: req.body.ownername } }).toArray((err, dataArray) => {
                        if (err) {
                            next(err)
                        }
                        else {
                            res.json({ data: dataArray })
                        }
                    })
                }
            })
        }
    })
})




module.exports = ownerdashboardroutes