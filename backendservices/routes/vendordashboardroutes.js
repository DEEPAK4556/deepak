const exp = require('express')
var vendordashboardroutes = exp.Router();

const authorization = require('../middleware/checkauthorization')
// importing dbconfig to adminroutes.js

const initdb = require("../dbconfig").initdb
const getdb = require("../dbconfig").getdb



initdb();
//post request handler for dopayment
vendordashboardroutes.post('/dopayment', authorization, (req, res) => {
    console.log(req.body)
    var dbo = getdb();
    dbo.collection('dopayment').insertOne(req.body, (err, success) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ message: success })
        }
    })
})



vendordashboardroutes.put('/addprofile', authorization, (req, res) => {
    console.log(req.body)

    var dbo = getdb();
    dbo.collection('vendor').updateOne({ name: { $eq: req.body.name } }, {
        $set: {
            dob: req.body.dob,
            address: req.body.address,
            email: req.body.email,
            mobile: req.body.mobile,
        }
    }, (err, success) => {

        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {

            res.json({ message: 'successfully updated' })
        }
    })


})

//get request handler for payment history in vendor

vendordashboardroutes.get('/paymenthistory', (req, res) => {

    var dbo = getdb();
    dbo.collection('dopayment').find().toArray((err, data) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            res.json({ message: data })
        }
    })

})

//vendor instersted house post request 
vendordashboardroutes.post('/whom-to-let', (req, res, next) => {
    console.log(req.body)
    var dbo = getdb();
    if (req.body.length == 0) {
        res.json({ message: "no data get from client" })
    }
    else {
        dbo.collection("whomtolet").insertOne(req.body, (err, success) => {
            if (err) {
                console.log('error in saving data')
                next(err)
            }
            else {
                res.json({ message: "request sent  successfully" })
            }
        })
    }
})

//get request handler for for whomtolet in vendor

vendordashboardroutes.get('/whom-to-let', authorization, (req, res) => {
    var dbo = getdb();
    dbo.collection('addhouse').find().toArray((err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            res.json({ message: data })
            console.log(data)

        }
    })
})


module.exports = vendordashboardroutes