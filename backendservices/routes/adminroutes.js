//installing and importing express module for adminroutes
const exp = require("express")
const adminroutes = exp.Router()

//connecting and initializing the database with route
const initdb = require('../dbconfig').initdb
const getdb = require('../dbconfig').getdb


// request handlers for admin routess

//post request handler for the admin login

adminroutes.post('/adlogin', (req, res) => {
    var dbo = getdb();
    dbo.collection('admin').find({ name: { $eq: req.body.name } }).toArray((err, dataArray) => {

        if (dataArray.length == 0) {
            res.json({ message: "invalid admin name" })
        }
        else {
            if (dataArray[0].name == req.body.name) {
                res.json({ message: "logged In Successfully" })
            }
            else {
                res.json({ message: "invalid password" })
            }
        }
    })
})

// get request handlers for allprofiles

adminroutes.get("/allprofiles", (req, res) => {
    var dbo = getdb();
    dbo.collection('owner').find().toArray((err, dataArray) => {
        if (err) {
            console.log('error in reading data')
        }
        else {
            dbo.collection('vendor').find().toArray((err, dataArray1) => {
                if (err) {
                    console.log('error in reading data')
                }
                else {
                    res.json({ data: dataArray, data1: dataArray1 })
                }
            })
        }
    })
})

module.exports = adminroutes