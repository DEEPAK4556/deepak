const exp = require('express')
var app = exp();
var userroutes = exp.Router();

var bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')

const secretKey = "secret"


// importing dbconfig to adminroutes.js

const initdb = require("../dbconfig").initdb
const getdb = require("../dbconfig").getdb



initdb();

//get request for update of editprofile in owner



// request handlers for admin routes

userroutes.post('/register', (req, res) => {

    var dbo = getdb();
    if (req.body.x == 'owner') {
        dbo.collection("owner").find({ name: { $eq: req.body.name } }).toArray((err, dataArray) => {
            if (dataArray.length == 0) {


                bcrypt.hash(req.body.password, 5, (err, hashedpassword) => {
                    let transporter = nodemailer.
                        createTransport({
                            service: "gmail",
                            auth: {
                                user: "dmudugonda@gmail.com",
                                pass: "9849062293"
                            }
                        });
                    let info = transporter.sendMail({
                        //sender address
                        from: '"login details" <dmudugonda@gmail.com>',
                        //list of recivers
                        to: req.body.email,
                        subject: "owner credentials",//subject line
                        text: `username: ${req.body.name},password: ${req.body.password}`,//plain text body
                        //html:"<b>hiii ra praveen</b>"//htmlbody
                    });
                    req.body.password = hashedpassword


                    dbo.collection('owner').insertOne(req.body, (err, success) => {
                        if (err) {
                            console.log("error in connecting database")
                            console.log(err)
                        }
                        else {
                            res.json({ message: "success" })
                        }
                    })
                })

            }
            else {
                res.json({ message: "already exists" })
            }
        })
    }
    else {
        dbo.collection('vendor').find({ name: { $eq: req.body.name } }).toArray((err, vendorArray) => {
            if (vendorArray.length == 0) {
                bcrypt.hash(req.body.password, 5, (err, hashedpassword) => {
                    let transporter = nodemailer.
                        createTransport({
                            service: "gmail",
                            auth: {
                                user: "dmudugonda@gmail.com",
                                pass: "9849062293"
                            }
                        });
                    let info = transporter.sendMail({
                        //sender address
                        from: '"login details" <dmudugonda@gmail.com>',
                        //list of recivers
                        to: req.body.email,
                        subject: "vendor credentials",//subject line
                        text: `username: ${req.body.name},password: ${req.body.password}`,//plain text body
                        //html:"<b>hiii ra praveen</b>"//htmlbody
                    });
                    req.body.password = hashedpassword
                    dbo.collection('vendor').insertOne(req.body, (err, success) => {
                        if (err) {
                            console.log("error in connecting database")
                            console.log(err)
                        }
                        else {
                            res.json({ message: "success" })
                        }
                    })

                })
            }
            else {
                res.json({ message: " exists" })
            }
        })
    }
})


userroutes.post('/login', (req, res, next) => {

    var dbo = getdb();



    if (req.body.user == 'owner') {
        dbo.collection('owner').find({ name: { $eq: req.body.name } }).toArray((err, data) => {




            if (data.length == 0) {
                res.json({ "message": "entered wrong username" })
            }

            else {
                bcrypt.compare(req.body.password, data[0].password, (err, result) => {

                    console.log(data)
                    if (result == true) {
                        const signedtoken = jwt.sign({ name: data[0].name }, secretKey, { expiresIn: "5000" });
                        console.log(signedtoken)
                        res.json({ "message": "owner successfully logged in", token: signedtoken, ownerdata: data })

                    }

                    else {
                        res.json({ 'message': "invalid owner password" })
                    }
                })
            }
        })
    }


    else if (req.body.user == 'vendor') {
        dbo.collection('vendor').find({ name: { $eq: req.body.name } }).toArray((err, data) => {




            if (data.length == 0) {
                res.json({ "message": "entered wrong vendor username" })
            }

            else if (req.body.user == 'vendor') {
                bcrypt.compare(req.body.password, data[0].password, (err, result) => {

                    console.log(data)
                    if (result == true) {
                        const signedtoken = jwt.sign({ name: data[0].name }, secretKey, { expiresIn: "5000" });
                        console.log(signedtoken)
                        res.json({ "message": "vendor successfully logged in", token: signedtoken, vendordata: data })

                    }

                    else {
                        res.json({ 'message': "invalid vendor password" })
                    }
                })
            }
        })
    }

    else {
        res.json({ message: "select usertype" })
    }

})


const accountSid = 'AC545dd81bef1d10de6ac0dd8538045e74';
const authToken = '6b3dffc282d80d92c88e41386e018034';
const client = require('twilio')(accountSid, authToken);




userroutes.post('/forgetpassword', (req, res, next) => {
    console.log(req.body)
    var dbo = getdb();
    if (req.body.usertype == 'owner') {
        dbname = 'owner'
    }
    else {
        dbname = 'vendor'
    }
    dbo.collection(dbname).find({ name: req.body.name }).toArray((err, userArray) => {
        if (err) {
            next(err)
        }
        else {
            if (userArray.length === 0) {
                res.json({ message: "user not found" })
            }
            else {

                jwt.sign({ name: userArray[0].name }, secretKey, { expiresIn: '7d' }, (err, token) => {
                    if (err) {
                        next(err);
                    }
                    else {
                        var OTP = Math.floor(Math.random() * 99999) + 11111;
                        console.log(OTP)

                        client.messages.create({
                            body: OTP,
                            from: '+12052739108', // From a valid Twilio number
                            to: '+91' + userArray[0].mobile,  // Text this number

                        })
                            .then((message) => {
                                dbo.collection('OTPCollection').insertOne({
                                    OTP: OTP,
                                    name: userArray[0].name,
                                    OTPGeneratedTime: new Date().getTime() + 30000
                                }, (err, success) => {
                                    if (err) {
                                        next(err)
                                    }
                                    else {
                                        res.json({
                                            "message": "user found",
                                            "token": token,
                                            "OTP": OTP,
                                            "userName": userArray[0].name
                                        })
                                    }
                                })
                            });

                    }

                })
            }
        }
    })
})

//verify OTP
userroutes.post('/otp', (req, res, next) => {
    console.log(req.body)
    var dbo = getdb();
    console.log(new Date().getTime())
    var currentTime = new Date().getTime()
    dbo.collection('OTPCollection').find({ "OTP": req.body.OTP }).toArray((err, OTPArray) => {
        if (err) {
            next(err)
        }
        else if (OTPArray.length === 0) {
            res.json({ "message": "invalidOTP" })
        }
        else if (OTPArray[0].OTPGeneratedTime < currentTime) {
            res.json({ "message": "invalidOTP" })
        }
        else {

            dbo.collection('OTPCollection').deleteOne({ OTP: req.body.OTP }, (err, success) => {
                if (err) {
                    next(err);
                }
                else {
                    console.log(OTPArray)
                    res.json({ "message": "verifiedOTP" })
                }
            })
        }
    })
})

//changing password
userroutes.put('/changepassword', (req, res, next) => {
    console.log(req.body)
    var dbo = getdb();
    if (req.body.usertype == 'owner') {
        dbname = 'owner'
    }
    else {
        dbname = 'vendor'
    }
    bcrypt.hash(req.body.password, 5, (err, hashedPassword) => {
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
            dbo.collection(dbname).updateOne({ name: req.body.name }, {
                $set: {
                    password: hashedPassword
                }
            }, (err, success) => {
                if (err) {
                    next(err)
                }
                else {
                    res.json({ "message": "password changed" })
                }
            })
        }
    })

})







app.use((err, req, res, next) => {
    console.log(err)
})
module.exports = userroutes

