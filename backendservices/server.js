const exp = require("express")
const app = exp()
const bodyparser = require('body-parser')
app.use(bodyparser.json());


//importing and using adminroutes and ownerroutes

const userroutes = require('./routes/userroutes')
app.use("/home", userroutes)

//importing and using ownerdashboard routes

const ownerdashboardroutes = require('./routes/ownerdashboardroutes')
app.use("/owner", ownerdashboardroutes)

//importing and using vendordashboard routes

const vendordashboardroutes = require('./routes/vendordashboardroutes')
app.use("/vendor", vendordashboardroutes)

//importing and using admin routes

const adminroutes = require('./routes/adminroutes')
app.use('/admin', adminroutes)


//installing and configuring path
const path = require('path')

app.use(exp.static(path.join(__dirname, '../dist/hrms')));




//assigning port Number


app.listen(process.env.PORT || 8080, () => {
    console.log(`server started`)
})