const mc = require('mongodb').MongoClient
const url = "mongodb://deepak:deepak@cluster0-shard-00-00-bxjkd.mongodb.net:27017,cluster0-shard-00-01-bxjkd.mongodb.net:27017,cluster0-shard-00-02-bxjkd.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority"
var dbo;

function initdb() {
    mc.connect(url, { useNewUrlParser: true }, (err, client) => {
        if (err) {
            console.log("error in connecting database")
            console.log(err)
        }
        else {
            dbo = client.db("test4")

            console.log("database is connected")
        }

    })



}
function getdb() {
    console.log("dbo has not initialized")
    return dbo;
}

module.exports = { initdb, getdb };