const jwt = require ('jsonwebtoken')
const secretKey="secret"
var checkAuthorization =(req,res,next)=>{
    
    
    //read authorization in request object

    var token =req.headers["authorization"]
    console.log(token)

    // if token found check for validity
    if(token==undefined)
    {
        res.json({message:"unauthorized access"})
    }
 
    else if(token.startsWith('Bearer '))

    {
          token=token.slice(7,token.length);

          jwt.verify(token,secretKey,(err,decoded)=>{
              if(err)
              {
                  return res.json({message:"invalid access"})
              }
              //forward to next middleware or request handler
              else{
                  next();
              }
          })
    }
}
  module.exports=checkAuthorization;